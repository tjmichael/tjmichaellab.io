---
layout: page
title: "Bookshelf"
permalink: /bookshelf/
---

![bookshelf](/assets/bookshelf.png)

## Now Reading

* **Game Feel: A Game Designer's Guide to Virtual Sensation**, Steve Swink
* **Game Engine Black Book: Wolfenstein 3D**, Fabien Sanglard
* **The Art of Computer Game Design**, Chris Crawford

## Read

* **Architectural Approach to Level Design: Second edition**, Christopher W. Totten
* **Spelunky**, Derek Yu
* **IF Theory Reader**, Kevin Jackson-Mead, J. Robinson Wheeler, [Review](/2019/07/27/IF-Theory-Reader/)
* **Queer Game Studies**, Bonnie Ruberg, Adrienne Shaw
* **A Theory of Fun for Game Design**, Raph Koster
* **Level Up!: The Guide to Great Video Game Design**, Scott Rogers
* **Embed with Games: A Year on the Couch with Game Developers**, Cara Ellison, [Review](/2019/07/17/Embed-with-Games/)
* **ZZT**, Anna Anthropy
* **Masters of Doom: How Two Guys Created an Empire and Transformed Pop Culture**, David Kushner
* **Blood, Sweat, and Pixels: The Triumphant, Turbulent Stories Behind How Video Games Are Made**, Jason Schreier

## Waiting

* **Game Engine Black Book: DOOM**, Fabien Sanglard
* **Zones of Control: Perspectives on Wargaming**, Pat Harrigan, Matthew G. Kirschenbaum 

## Abandoned

* **Foundations of Game Engine Development, Volume 1: Mathematics**, Eric Lengyel 