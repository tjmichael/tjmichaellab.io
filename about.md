---
layout: page
title: "About"
permalink: /about/
---

**TJ Michael** Is a game / simulation developer from Israel. He knows C++, C#, Unity, and can cook some things. 

He is also secretly a time traveling cat from roman times.

You can say hi to TJ by sending an e-mail to: hello **at** tjmichael.dev