---
layout: post
title:  "IF Theory Reader | Kevin Jackson-Mead, J. Robinson Wheeler"
date:   2019-07-27 10:00:00 +0300
categories:
 - books
 - Interactive Fiction
 - Kevin Jackson-Mead
 - J. Robinson Wheeler
---

The "IF Theory Reader" is a time-capsule. Not just a snapshot of history, of the road to where we are now, but also a lifeboat, a beacon of a community that no longer exists in the form that it once did. An exhibition of those theories and ideas that *were* developed and iterated on, and also those ideas that (for whatever reason) weren't.

---

Ages ago, I use to be a real nuisance in the Sonic [fangame](https://en.wikipedia.org/wiki/Fangame) community. I was young, driven by a mix of naiveté, stupidity, and that special kind of [Dunning–Kruger](https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect) mentality that comes from a hefty does of privilege, isolation, and undiagnosed obsessive tendencies.

Some of the people I annoyed at the time went on to do great things, heck, the guy who eventually drove me off of what was then the biggest fangaming board went on to design some of Sonic Mania's best levels. A bunch of the rest... well, a quick Linkedin search full of internships and low-income gigs makes me wonder how much the fact I actually ended up where I did has to do with that aforementioned privilege.

The "IF Theory Reader" isn't a book about my community, it's something that it never had, something with the potential to outlive the community that birthed it.

The "IF Theory Reader" is a collection of essays, written by different members of the interactive fiction (IF) community in the early oughts, revised and reconstituted throughout the decade, and finally canonised in 2011.

At the time the IF scene was mostly hobbyist, long gone where the days of infocom with their Zorks and Hitchiker's Guides. Untethered by commercial considerations of the older scene, it managed to produce a unique assortment of games, with its own assortment of celebrities.

Figures like Andrew Plotkin, Graham Nelson, or Emily Short might not have come to mind to the oughts designer as often as the likes of Warren Spector or Shigeru Miyamoto, but here it is *they* who cast their shadow over the histories and theory.

Maybe it's because I'm projecting, because so little of substance remains of the Sonic fangaming world I had come from, but I'm so worried about the survival of the "IF Theory Reader".

Sure the copy I have on my shelf is a real book, real in the sense that it has pages, that it's ink printed on paper, but It's not *real real* in the sense where it has a publisher and an ISBN. Even the copy I do have is full of pages printed at varying qualities, its cover already fraying after just one read.

There's a canon of theory here, a peek into the crucible that was essentially a quickly iterating and highly literate community of game designers.

The interactive fiction community is (like the Sonic community I had belonged to) a community of creators, where those playing the games were also those making them. These communities have had a tendency to show up a bunch in my reading so far, from "ZZT" to those late ought days of "TIGSource" that birthed the likes of Spelunky. However (for the most part), this is the first example I've found where this isn't as a historical record, rather a theoretical one.

I urge you to read this book, not only because getting more eyes on it is more likely to lead to it's longer survival, but because it's so hard to tell if there will ever again be an interactive fiction community of the early 2000s, because this is the only place where you'll find the knowledge *those specific conditions* were necessary to create. 

The "IF Theory Reader" is precious, for the meantime at-least, there are so other few books like it. Maybe it can be a model for preserving the essence of those other lost eras, perhaps for saving what their successors are doing now. Sure enough, interactive fiction, Sonic fangames, and TIGSource haven't gone anywhere. They've all changed and mutated, the people may have changed (either personally or literally), but a continuity still exists to those earlier days.

---

As previously mentioned, the "IF Theory Reader" doesn't actually have an ISBN, and isn't available from any regular online booksellers. The PDF can be [downloaded for free online](https://inform-fiction.org/manual/download_if_theory.html?fbclid=IwAR1ZDex4Vaa8Mun3PVcptnz-DJKG2MaFZ3J6obRPb2Fu5-Euanbt_wJPfOo), but can also be printed out in book form via an official [Lulu print-on-demand option](http://www.lulu.com/product/paperback/if-theory-reader/15325304). I have to admit, as much as I love the book, the Lulu printing was spotty in some parts, and the cover is already peeling apart, if anyone knows of a superior print-on-demand service, please let me know and I'll gladly give it a shot.