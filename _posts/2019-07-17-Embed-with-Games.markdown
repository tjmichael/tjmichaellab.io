---
layout: post
title:  "Embed with Games: A Year on the Couch with Game Developers | Cara Ellison"
date:   2019-07-17 23:02:35 +0300
categories:
 - books 
 - Cara Ellison
---

I’d be lying if I didn’t admit how jealous I am of Cara Ellison.

There's a version of the story, where me wanting to be a game developer starts with "Mega Man Anniversary Collection" for the <span style="color:#7851a9">**Gamecube**</span>. It's 2004, I'm a socially awkward Israeli kid living in the states, and I watch the [episode of G4's Icons that came bundled with the disc](https://youtu.be/wSBwfe-Fu48). I realise people make games, games aren't just another thing the kids at school pick on you for playing, they can be a vocation. The people who make games become my heroes.

So of course I’d be jealous, who wouldn’t if Cara Ellison got to hang out with a bunch of their idols?

That’s what “Embed with Games: A Year on the Couch with Game Developers” is about *really*. Cara Ellison travels the world, cohabiting and hanging out with game developers, living a life I could only dream of, and then selling me that experience as a book.

Except, that’s not that truth, that's that 10 year old kid in 2004 doing the reading. 

---

Back in 2016 [Rami Ismail](https://twitter.com/tha_rami) came to give a talk at the yearly Israeli Game Developers conference, **Gameis**. Rami is a prolific indie developer and speaker from the Netherlands, of all the speakers, he was the one me and my colleagues were most excited to see that day.

Despite hating that cramped type of Israeli bar the event’s after parties were usually held at, I loved attending them. I’d always try to stay until the very end, strike up a conversation with the people I considered my tribe, meet the sorts of people I had hoped to become.

Now in 2016, this was my chance not only to meet and talk to someone who actually worked on games, but someone who worked on the sorts of games I played, someone I looked up to.

Except I couldn’t. I was frozen, instead of walking up and well, trying to talk to Rami, I just stood there, staring at him from across the bar. ***For a couple of hours***.

I’ve since sent an apology to Rami for spending the night stalking him like an inexperienced serial killer about to strike. But this apology came a year or two after the fact, after my anxiety became a big enough issue in my life where **I** finally had to take responsibility for myself, instead of leaving it to others to deal with it.

The cause of that anxiety wasn’t just meeting my hero, it was the *why* of why I wanted to meet him in the first place. That episode of “Icons” didn’t just show me that people made games, it gave me hope. I was a lonely kid, I was crippled by an anxiety I didn’t understand know how to deal with. I couldn’t recognise it, I couldn’t see past my own head that what had been crippling me socially wasn’t my “nerdy” tastes, it was me. I didn't look up to game developers because I thought they were proud to be like me, I looked up to them because I thought that they would be my friends.

Seeing Rami Ismail there, that para-social fantasy collided with a cold reality. I wasn’t alone because I was “nerd” ostracised by a world of shallow “popular kids”. Here I was among peers, about to meet the sort of person I had for years fantasised about befriending, and yet I couldn’t move forward.

---

I’m jealous of Cara Ellison because my own knee jerk reaction to “Embed with Games…” sees it as a book about how she got to live out that fantasy, be friends with the very people I had so long fantasised to fraternise with. That however, is doing a horrible disservice to the work, it’s again pawning my own issues onto someone else’s experience. It's making out what is explicitly another person's experience be about me.

Ellison's experience here is a lens, strike that: it's a series of lenses, through which the author brings a unique perspective to the world of games. Every essay that makes up the work is a unique encounter, with a unique person written in a unique style.

“Embed…” is a portrait of the world of game developers, from southeastern Asia, to France, to American expats exiled back to their native lands. It’s a global look at the motivations, lives, passions and relationships of the people making games today.

The work isn’t an experiment, not the same set of questions repeated throughout with different people. Each essay is in its own format, a travel log, a profile, an intimate conversation. Yet “Embed…” manages to avoid feeling like a kaleidoscope, showing a twisted view of the world through a shattered lens. It’s a quilt, it’s a pedigree of the best parts of our weird and wonderful industry.

I bought my own copy of “Embed with Games...” from [Blackwell’s](https://blackwells.co.uk/bookshop/product/9781846973444?productTitle=&quantity=6&author=&quantity=6&publisher=&quantity=6&quantity=6&pubDateFrom=&pubDateTo=&isbn=1846973449&priceFrom=&priceTo=&academicLevel=E) via [Abebooks](https://www.abebooks.com/products/isbn/9781846973444?cm_sp=bdp-_-ISBN10-_-PLP). Since the book is no longer in stock on Blackwell’s website, I’ll also link to the [Amazon page](https://www.amazon.com/Embed-Games-Year-Couch-Developers/dp/1846973449) where I bought a copy for kindle.